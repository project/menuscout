Type in a keyword or two, and get a list of matching admin pages back. Links are highlighted on the current page with spiffy bubbles. Deep links are highlighted with half-transparent bubbles.

Updated demo video available: http://acko.net/files/Drupal%20Menu%20Scout.mp4

It indexes the menu item titles, descriptions and help with search.module on first search (takes a couple of seconds). It does not use cron-based indexing.

Known limitations
=================
- Your admin theme must have a 'content' region. This is standard for all core themes.
- Requires javascript (degrades gracefully otherwise).

Credits
============

Authored by Steven Wittens