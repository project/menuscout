
/**
 * Attaches the guide behaviour to all required fields.
 */
Drupal.guideAutoAttach = function () {
  var gdb = new Drupal.ACDB(Drupal.settings.guide.url);
  $('input.guide').each(function () {
    var uri = this.value;
    var input = $(this)
      .attr('autocomplete', 'OFF')[0];
    $(input.form).submit(Drupal.guideSubmit);
    new Drupal.guide(input, gdb);
  });

  // Manually add body CSS for IE so that $('body').css('height') works.
  if (jQuery.browser.msie) {
    $('body').css('height', '100%').css('left', '0');
  }
/*
  // Move the form to the top of the page
  $('#menuscout-guide-form').clone().id('menuscout-guide-new').insertAfter("div.help").show();
  // Remove the old form so that IDs don't conflict.
  $('#menuscout-guide-form').remove();
  */
}

/**
 * Prevents the form from submitting if the suggestions popup is open
 * and closes the suggestions popup when doing so.
 */
Drupal.guideSubmit = function () {
  $('#autocomplete').each(function () {
    this.owner.hidePopup();
  });
  return false;
}

/**
 * A guide object
 */
Drupal.guide = function (input, db) {
  var guide = this;
  this.input = input;
  this.db = db;

  $(this.input)
    .keydown(function (event) { return guide.onkeydown(this, event); })
    .keyup(function (event) { guide.onkeyup(this, event) })
    .blur(function () { guide.hidePopup(); guide.db.cancel(); });

};

/**
 * Inherited methods from autocomplete.
 */
Drupal.guide.prototype.onkeydown = Drupal.jsAC.prototype.onkeydown;
Drupal.guide.prototype.onkeyup = Drupal.jsAC.prototype.onkeyup;
Drupal.guide.prototype.selectDown = Drupal.jsAC.prototype.selectDown;
Drupal.guide.prototype.selectUp = Drupal.jsAC.prototype.selectUp;
Drupal.guide.prototype.highlight = Drupal.jsAC.prototype.highlight;
Drupal.guide.prototype.unhighlight = Drupal.jsAC.prototype.unhighlight;
Drupal.guide.prototype.setStatus = Drupal.jsAC.prototype.setStatus;

/**
 * Puts the currently highlighted suggestion into the guide field and
 * visits the target URL.
 */
Drupal.guide.prototype.select = function (node) {
  this.input.value = node.guideValue;
  location.href = node.guideUrl;
}

/**
 * Hides the guide suggestions
 */
Drupal.guide.prototype.hidePopup = function (keycode) {
  // Hide popup
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function() { $(popup).remove(); });
  }

  // Unexpose all items
  this.unexpose();

  // Select item if the right key or mousebutton was pressed
  if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
    this.select(this.selected);
  }

  // Remove selection
  this.selected = false;
}

/**
 * Positions the suggestions popup and starts a search
 */
Drupal.guide.prototype.populatePopup = function () {
  // Show popup
  if (this.popup) {
    $(this.popup).remove();
  }
  this.selected = false;
  this.popup = document.createElement('div');
  this.popup.id = 'autocomplete';
  this.popup.className = 'guide-popup';
  this.popup.owner = this;
  $(this.popup).css({
    marginTop: this.input.offsetHeight +'px',
    width: (this.input.offsetWidth - 4) +'px',
    display: 'none'
  });
  $(this.input).before(this.popup);

  // Do search
  this.db.owner = this;
  this.db.search(this.input.value);
}

/**
 * Fills the suggestion popup with any matches received
 */
Drupal.guide.prototype.found = function (matches) {
  // Unhighlight everything in the page.
  this.unexpose();

  // Prepare matches
  var ul = document.createElement('ul');
  var ac = this;
  var i = 0;
  for (key in matches) {
    var li = document.createElement('li');
    $(li)
      .html('<div>'+ matches[key][0] +'</div>')
      .mousedown(function () { ac.select(this); })
      .mouseover(function () { ac.highlight(this); })
      .mouseout(function () { ac.unhighlight(this); });
    li.guideValue = key;
    li.guideUrl = matches[key][1];
    $(ul).append(li);

    // Highlight this link in the page.
    this.expose(key);
    i++;
  }

  // Show popup with matches, if any
  if (this.popup) {
    if (ul.childNodes.length > 0) {
      $(this.popup).empty().append(ul).show();
    }
    else {
      $(this.popup).css({visibility: 'hidden'});
      this.hidePopup();
    }
  }
}

/**
 * Unexpose all items on the page.
 */
Drupal.guide.prototype.unexpose  = function () {
  // Remove shade.
  $('#guide-shade').remove();

  // Remove bubbles.
  if (this.exposed && this.exposed.length > 0) {
    $(this.marked).removeClass('exposed');
    $(this.exposed).remove();    
    this.exposed = [];
    this.marked = [];
  }
}

/**
 * Expose links to this
 */
Drupal.guide.prototype.expose = function (path) {
  var guide = this;
  // Dim the page.
  if ($('#guide-shade').size() == 0) {
    $('<div id="guide-shade"></div>').appendTo($('body')).css({
      'position': 'absolute',
      'top': '0px',
      'left': '0px',
      'right': '0px',
      'height': parseInt($('body').css('height')) + 30 + 'px',
      'margin-bottom': '-30px',
      'background': '#000',
      'opacity': 0.0,
      'z-index': 10000
    })
    .animate({ 'opacity': 0.5 });
    this.exposed = [];
    this.marked = []
  }

  var z = 10001;
  /**
   * Highlight a DOM node.
   */
  var _expose = function (node, opacity) {
    // Check if it's been exposed already.
    if ($(node).is('.exposed') || $(node.parentNode.parentNode).css('float') != 'none') {
      return;
    }
    $(node).addClass('exposed');
    guide.marked.push(node);

    // Ensure we're positioning relative to the parent (prevents
    // shrink-wrapping of content).
    var p = node.parentNode;
    if ($(p).css('position') == 'static') {
      $(p).css('position', 'relative');
    }

    // Duplicate highlighted item.
    var clone = node.cloneNode(true);
    $(node).before(clone);
    var glow = node.cloneNode(true);
    $(node).before(glow);

    // Place above all else.
    $([glow, clone]).css({
      'position': 'absolute',
      'opacity': 0      
    });

    // Style and animate (hardcoded, to override).
    $(glow).css({
      'z-index': z++,
      'background': '#000',
      'padding': '12px 27px',
      'margin': '-10px -27px',
      'border-radius': '18px',
      '-moz-border-radius': '18px'
    })
    .animate({ 'opacity': opacity * 0.25 });
    $(clone).css({
      'z-index': z++,
      'background': '#fff',
      'border': '1px solid #ccc',
      'padding': '7px 22px',
      'margin': '-8px -23px',
      'border-radius': '14px',
      '-moz-border-radius': '14px'
    })
    .animate({ 'opacity': opacity });

    // Remember clone elements for later.
    guide.exposed.push(glow);
    guide.exposed.push(clone);
  }

  // Highlight exact matches.
  $('a[@href$='+ path +']').each(function () { _expose(this, 1) });

  // Highlight partial matches from 3 or more path components.
  var path = path.split('/');
  var part = '';
  for (i in path) {
    part += (part == '' ? '' : '/') + path[i];
    if (i >= 2) {
      $('a[@href$='+ part +']').each(function () { _expose(this, 0.5) });
    }
  }
}


// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.guideAutoAttach);
}

